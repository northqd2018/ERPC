cmake_minimum_required(VERSION 3.8)

add_executable(app4 app.c)
add_executable(service4 service.c)
target_link_libraries(app4 ${Libraries})
target_link_libraries(service4 ${Libraries})
